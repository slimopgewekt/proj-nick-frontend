import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {App} from './components';
import {store}  from './store';
import {http}  from './lib';
import config from './config/application';
import 'normalize.css/normalize.css';
import './assets/index.scss';

http.init(config.api);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
