import {httpPost, httpGet, httpPut, httpPatch, httpDelete, init} from './http';

export const http = {
    httpPost, httpGet, httpPut, httpPatch, httpDelete, init
};
