import {loginAction, logoutAction} from './user';

export const user = {
    loginAction,
    logoutAction
}
