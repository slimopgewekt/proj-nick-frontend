import types from '../types';

export const loginAction = (token, type, typeId) => {
    return {
        type: types.user.LOGIN, payload: {
            token,
            type,
            typeId
        }
    }
}

export const logoutAction = () => {
    return {
        type: types.user.LOGOUT
    }
}
