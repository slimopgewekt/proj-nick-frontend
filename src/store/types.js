const types = {
    user: {
        LOGOUT: "LOGOUT",
        LOGIN: "LOGIN",
        REFRESH: "REFRESH"
    }
};
export default types;
