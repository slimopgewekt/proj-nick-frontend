import storeInstance from './store';
import reducer from './reducers';
import {user} from './actions';

export const store = storeInstance;
export const rootReducer = reducer;
export const actions = {
    user
};
