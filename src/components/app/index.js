import React, {Component}  from 'react';
import {Layout, Router} from '../components';
import {BrowserRouter} from "react-router-dom";

export default class App extends Component {
    render() {
        return (
            <div className="App">
                <BrowserRouter>         
                    <Layout>        
                        <Router/>
                    </Layout>
                </BrowserRouter>
            </div>

        );
    }
};

