import React  from 'react';
import "./intropage.scss";
import {Col, Container, Row, Button} from "react-bootstrap";
import {Link} from 'react-router-dom';
import {useQuestions} from "../../hooks";

//images    
import WatJij from '../../assets/images/logo_wat_jij.jpg';


export default (props) => {
    const {questions} = useQuestions();
    //const {setAnswer} = useQuestion(1);
    return (
        <div className="IntroPage">
            <Container bsPrefix="container">
                <Row>
                    <Col sm={2}>
                        <Link to="/">
                            <img src={WatJij} alt="wat_jij" width="100" />
                        </Link>
                    </Col>
                    <Col sm={10}>
                        <div id="hero">
                            <p>Vraag 1</p> 
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col sm={2}></Col>
                    <Col sm={10}>
                        <div id="question">
                            <p>Je stapt op de fiets om naar school te gaan. Tegelijkertijd zie je de buurman net in zijn nieuwe elektrische auto stappen, hij rijdt er geruisloos mee weg.</p>
                            <p>Je ziet steeds vaker elektrische auto’s op de weg rijden. Het is mooi dat de elektrische auto een schoner alternatief lijkt voor de benzineauto. Daarentegen wordt er wel vaak kinderarbeid in Afrika ingezet om de grondstoffen voor de autobatterijen te delven. Wat vind jij van elektrisch rijden?</p> 
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col sm={2}></Col>
                    <Col sm={5}>
                        <div id="answer1">
                            <Link to="/vraag">
                                <Button size='sm' variant='dark' onClick={() => {
                                }}>Antwoord 1</Button>
                            </Link>
                        </div>
                    </Col>
                    <Col sm={5}>
                        <div id="answer2">
                            <Link to="/vraag">
                                <Button size='sm' variant='dark' onClick={() => {
                                }}>Antwoord 2</Button>
                            </Link>
                        </div>
                    </Col>
                </Row>
                {
                    questions.map((question, index) => (
                        <p key={index}>
                            {question.title}
                        </p>
                    ))
                }
                <Button size='sm' variant='dark' onClick={() => {
                    //setAnswer(0);
                    props.history.push('/vraag/1/video')
                }}>Klik</Button>
            </Container>
        </div>
    )
};
