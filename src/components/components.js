import App from './app';
import IntroPage from './intropage';
import Layout from "./layout";                    // Layout is zelf verzonnen local name, bevat 'withRouter(Authenticated)'
import Router from './router';
import Question from './question';
import ResponsiveContainer from './responsivecontainer';

export {
    App,
    IntroPage,
    Layout,
    Router,
    Question,
    ResponsiveContainer,
}
