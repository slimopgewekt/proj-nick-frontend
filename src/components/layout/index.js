import React, {Fragment} from 'react';
import {NavLink, withRouter} from "react-router-dom";
import './layout.scss';


const Authenticated = ({children}) => {
    return (
        <div className="Layout">
            <header>
                <HeaderLinks />               // contains all the links for navigating through the website
            </header>
            <main>
                {children}
            </main>
            <footer>
                <FooterLinks />
            </footer>
        </div>
    )
};

const HeaderLinks = () => {
    return (
        <Fragment>
            <NavLink to={`/`} className="navlink bp3-minimal">Start</NavLink>
        </Fragment>
    )
}

const FooterLinks = () => {
    return (
        <Fragment>
            <p style={{textAlign: 'right', margin: 0}}>
                &copy; Slim Opgewekt!
            </p>
        </Fragment>
    )
}
export default withRouter(Authenticated);
