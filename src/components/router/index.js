import React from 'react';
import {Redirect, Route, Switch, withRouter} from "react-router-dom";
import {IntroPage, Question} from '../components';

const Router = () => (
    <Switch>
        <Route path="/" exact component={IntroPage} />
        <Route path="/vraag" exact component={Question} />
        <Route path="/vraag/:vraagnummer" exact component={Question} />
        <Route default render={() => (
            <Redirect to="/"/>
        )}/>
    </Switch>
);

export default withRouter(Router);
