import React  from 'react';
import "./responsivecontainer.scss";

export default ({children}) => (
    <div className="ResponsiveContainer">
        {children}
    </div>
);
