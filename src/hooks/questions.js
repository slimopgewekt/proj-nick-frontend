import { useState, useEffect } from 'react';
import {question} from "../api";

function useQuestions() {
    const [data, setData] = useState({questions: [], fetched: false});

    useEffect(() => {
        if(!data.fetched){
            (async () => {
                    const state = await question.listQuestions();
                    if(state.questions){
                        setData({questions: state.questions, fetched: true});
                    }
                }
            )();
        }
    });

    return {
		questions: data.questions
    };
}

export default useQuestions;
