
import {getQuestion, listQuestions} from './question'

export const question = {
    getQuestion,
    listQuestions
};
