import {http} from '../lib';

const getQuestion = async (questionId) => {
    try {
        const result = await http.httpGet(`/questions/${questionId}`,null,(err) => {
            throw err;
        });
        return result;
    }catch(err){
        return err;
    }
};

const listQuestions = async () => {
    try {
        const result = await http.httpGet(`/questions`, null, (err) => {
            throw err;
        });
        return result;
    }catch(err){
        return err;
    }
}

export {
	getQuestion,
	listQuestions
}
